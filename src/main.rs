use sha2::{Digest, Sha256};
use std::fs;
use std::io::Write;

fn main() {
    let entries = fs::read_dir(".").expect("Unable to read directory");

    // Open file to write the checksums
    let mut sums_file = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open("./SHA256SUMS")
        .expect("Unable to create file for checksums");

    for entry in entries {
        // Unwrap entry
        let unwrapped_entry = entry.unwrap();

        // Set variables
        let file_name = format!(
            "{}",
            unwrapped_entry
                .file_name()
                .into_string()
                .expect("Unable to convert OsString to String")
        );
        let is_dir = unwrapped_entry.file_type().unwrap().is_dir();

        // Open file to read
        if !is_dir && file_name != "SHA256SUMS" {
            // Create Sha256 object
            let mut hasher = Sha256::new();
            let file_path = unwrapped_entry.path();

            let file = fs::read(file_path).expect("Unable to read file");

            // Set input data
            hasher.input(file);

            // Read hash digest and consume hasher
            let result = hasher.result();

            // Format result to be written
            // Note: {:x} formats the u8 slice as characters
            let result_to_write = format!("{:x} {}\n", result, file_name);
            // Write result to file
            sums_file
                .write_all(result_to_write.as_bytes())
                .expect("Unable to write checksum to file");
        }
    }
}
